#!/bin/bash
# Copy configuration file of Cyclone DDS.
echo "Copy Cyclone DDS ..."
mkdir -p ~/cyclonedds
pushd ~/cyclonedds
wget https://gitlab.com/autowarefoundation/autoware_reference_design/-/raw/main/docs/Appendix/Open-AD-Kit-Start-Guide/cyclonedds/cyclonedds.xml
network_card=$(ip addr | grep 'xenbr[0-9]*\:' | head -1 | awk -F': ' '{print $2}')
if [ "$network_card" = "" ]; then
        network_card=$(ip addr | grep BROADCAST | head -1 | awk -F': ' '{print $2}')
fi
echo $network_card
sed -i "s/>lo</>$network_card</g" cyclonedds.xml
popd

# Copy kernel configuration file for tuning kernel parameters
echo "Setup kernel configuration ..."
pushd ~/
wget https://gitlab.com/autowarefoundation/autoware_reference_design/-/raw/main/docs/Appendix/Open-AD-Kit-Start-Guide/sysctl.d/60_cyclonedds.conf
# Update kernel parameters
sudo mv ~/60_cyclonedds.conf /etc/sysctl.d/
sudo sysctl -p /etc/sysctl.d/60_cyclonedds.conf
popd